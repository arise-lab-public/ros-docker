#!/bin/bash

# Setup Variables for building
source env.sh

cd .. || exit
docker compose -p "$DOCKER_PROJECT_NAME" up --detach