#!/bin/bash

source env.sh
cd .. || exit
xhost +SI:localuser:"$USER"
docker compose -p "$DOCKER_PROJECT_NAME" exec ros bash
xhost -SI:localuser:"$USER"