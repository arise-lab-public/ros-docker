#!/bin/bash

# Setup Variables for building
source env.sh

# XAUTH stuff for GPU
if [ ! -f $XAUTH ]
then
    touch $XAUTH
    xauth_list=$(xauth nlist :0 | sed -e 's/^..../ffff/')
    if [ ! -z "$xauth_list" ]
    then
        echo $xauth_list | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi
    chmod a+r $XAUTH
fi

# Navigate to Docker directory
cd .. || exit

# Build Docker Image
docker compose -p "$DOCKER_PROJECT_NAME" build