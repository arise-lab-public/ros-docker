#!/bin/bash

APP_USER=$USER
APP_USER_ID=$(id -u "$USER")

export APP_USER
export APP_USER_ID
export UBUNTU_VERSION=focal
export ROS_VERSION=noetic
export HOST_SRC=/home/$USER/catkin_ws/src
export DOCKER_SRC=/home/$USER/catkin_ws/src
export DOCKER_PROJECT_NAME=ros-docker
export HOSTNAME=${HOSTNAME}
export XAUTH=/tmp/.docker.xauth