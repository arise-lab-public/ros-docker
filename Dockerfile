#!/bin/bash

# Set VERSION
ARG UBUNTU_VERSION

# Use ros docker image
FROM osrf/ros:noetic-desktop-full

# Arguments
ARG APP_USER
ARG USER_ID
ARG KEY_VERSION
ARG ROS_VERSION

# Set Shell TYpe
SHELL ["/bin/bash", "-c"]

# Set Timezone and make install non-interactive
# -- This is needed to prevent be prompted for Geographic region
ENV TZ=US/Mountain \
    DEBIAN_FRONTEND=noninteractive

ENV XDG_RUNTIME_DIR="/tmp/runtime-root" \
    NO_AT_BRIDGE=1

ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# Create App User
RUN groupadd -r $APP_USER
RUN useradd -r -g $APP_USER $APP_USER
RUN usermod -u $USER_ID $APP_USER
RUN usermod -aG sudo $APP_USER
RUN echo "$APP_USER    ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

RUN mkdir /home/${APP_USER} && \
    chown -R ${APP_USER}:${APP_USER} /home/${APP_USER}

# Install Supporting Software
RUN apt-get update && \
    apt-get install -y curl \
    locales \
    lsb-release \
    cmake \
    g++ \
    git \
    gnupg gnupg1 gnupg2 \
    libcanberra-gtk* \
    python3-catkin-tools \
    python3-pip \
    python3-tk \
    wget \
    vim \
    emacs \
    nano \
    dos2unix \
    python3.8 \ 
    python-is-python3 \ 
    evince \
    ros-${ROS_VERSION}-plotjuggler-ros

# Update Ubuntu
RUN apt-get update

RUN apt-get install -y ros-${ROS_VERSION}-slam-gmapping
RUN apt-get install -y ros-${ROS_VERSION}-dwa-local-planner
RUN apt-get install -y ros-${ROS_VERSION}-tf2-tools

# Install turtlebot3
RUN apt-get update && apt-get install -y ros-${ROS_VERSION}-joy ros-${ROS_VERSION}-teleop-twist-joy \
  ros-${ROS_VERSION}-teleop-twist-keyboard ros-${ROS_VERSION}-laser-proc \
  ros-${ROS_VERSION}-rgbd-launch ros-${ROS_VERSION}-rosserial-arduino \
  ros-${ROS_VERSION}-rosserial-python ros-${ROS_VERSION}-rosserial-client \
  ros-${ROS_VERSION}-rosserial-msgs ros-${ROS_VERSION}-amcl ros-${ROS_VERSION}-map-server \
  ros-${ROS_VERSION}-move-base ros-${ROS_VERSION}-urdf ros-${ROS_VERSION}-xacro \
  ros-${ROS_VERSION}-compressed-image-transport ros-${ROS_VERSION}-rqt* ros-${ROS_VERSION}-rviz \
  ros-${ROS_VERSION}-gmapping ros-${ROS_VERSION}-navigation ros-${ROS_VERSION}-interactive-markers \
  ros-${ROS_VERSION}-turtle-tf2 ros-${ROS_VERSION}-tf2-tools ros-${ROS_VERSION}-tf \
  ros-${ROS_VERSION}-dynamixel-sdk ros-${ROS_VERSION}-turtlebot3-msgs ros-${ROS_VERSION}-turtlebot3

# Setup bashrc
COPY include/bashrc.txt /home/$APP_USER/.bashrc
RUN chown $APP_USER:$APP_USER /home/$APP_USER/.bashrc
RUN chmod u+rx /home/$APP_USER/.bashrc
RUN chmod go+r /home/$APP_USER/.bashrc

# Switch to App User
USER ${APP_USER}

RUN python3 -m pip install numpy matplotlib scipy scikit-learn opencv-python

# Setup Workspace
RUN mkdir -p /home/$APP_USER/catkin_ws/src && \
    source /opt/ros/${ROS_VERSION}/setup.bash && \
    cd /home/$APP_USER/catkin_ws && \
    catkin_make

RUN mkdir /tmp/runtime-root && \
    chmod 700 /tmp/runtime-root

# Add some helpful aliases
RUN echo "alias catkin_source='source /opt/ros/$ROS_VERSION/setup.bash && source /home/$APP_USER/catkin_ws/devel/setup.bash'" >> ~/.bashrc

# Set Volume Directory
RUN chown -R $APP_USER:$APP_USER ~/catkin_ws
VOLUME /home/$APP_USER/catkin_ws/src
WORKDIR /home/$APP_USER/catkin_ws