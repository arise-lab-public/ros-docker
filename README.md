# ROS Docker

## PreReqs

    Docker must be installed. https://www.docker.com/get-started/

    For MacOS, X11 Must be installed.

        Via Brew:
        brew install --cask xquartz

        Via Download:
        https://www.xquartz.org/

## Notes

After building, it may be necessary to set the permissions on the catkin_ws directory:

    For Linux:

       sudo chown -R $USER:docker /home/$USER/catkin_ws

    For Mac:

       sudo chown -R $USER:docker /Users/$USER/catkin_ws

On a Mac, you will need to share the catkin_ws directory with Docker.

    Docker -> Preferences -> Resources -> File Sharing

    Add /Users/$USER/catkin_ws to the list of shared directories


## TL;DR

    Build Docker Image and start Container on Linux: 
        
        cd /home/$USER
        mkdir catkin_ws
        git clone git@git.cs.du.edu:arise-lab/ros-docker.git
        cd ~/ros-docker/scripts
        ./build.sh
        sudo chown -R $USER:docker /home/$USER/catkin_src
        ./start.sh
        ./terminal.sh
        catkin_source

## Scripts

Environment Setup (`env.sh`):

    Sets the values for variables used in building the docker image.

    - UBUNTU_VERSION - Set the version of ubuntu (focal)
    - ROS_VERSION - Verfion of ROS (noetic)
    - HOST_SRC - The path to the catkin workspace on the host (/home/$USER/catkin_ws)
    - DOCKER_SRC - The path to the catkin workspace on the docker image (/home/$USER/catkin_ws)
    - DOCKER_PROJECT_NAME - The name that is to be assigned to the docker image (ros-docker)
    - APP_USER - The user to be used within the docker image ($USER)
    - APP_ID - The id of the user on the system (id $USER)

Environment Setup (`env-mac.sh`):

    Same as `env.sh` but sets HOST_SRC to a Mac OS path

Build Docker (`build.sh`):

    Builds the docker image with ROS installed on UBUNTU.

Start Docker Container (`start.sh`):
    
    Starts the docker container 

Stop Docker Container (`stop.sh`):
    
    Stops the docker container

Start Terminal (`terminal.sh`):

    Starts a terminal in the docker image



